#!/usr/bin/env bash

function cleanup
{
    echo cleanup
}

trap cleanup EXIT
cleanup

#if [ ! -d dockviz ] ; then
#    git clone https://github.com/justone/dockviz.git
#fi
#cd dockviz
docker build -t dockdot .

# (se f.eks. images med ”eog images.png”. eog er en image viewer)
docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz images -d | docker run --rm -i --name=dockdot -v "$PWD":/input dockdot -Tpng -o images.png
#dockviz images -d | dot -Tpng -o images.png
#dockviz containers -d | dot -Tpng -o containers.png
docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz containers -d | docker run --rm -i --name=dockdot -v "$PWD":/input dockdot -Tpng -o containers.png
