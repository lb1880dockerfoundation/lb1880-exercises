#!/usr/bin/env bash

source ././../../common.sh

function setup
{
    docker run -d --name killMe jenkins:$JENKINS_TAG

    docker run -d --name killMeToo jenkins:$JENKINS_TAG
    docker run -d --name killMeTheHardWay jenkins:$JENKINS_TAG
    docker stop killMe

    docker run -d jenkins:$JENKINS_TAG
    docker run -d jenkins:$JENKINS_TAG
    docker run -d jenkins:$JENKINS_TAG
}

setup

echo Slet en stoppet container
docker rm killMe

echo Slet en kørende container
docker stop killMeToo
docker rm killMeToo

echo Alternativt
docker rm -f killMeTheHardWay

echo Slet alt
docker rm -f $(docker ps -aq)

function setup2() {
    docker rm -f exitMeAndKillMeLater || true
    docker run --name exitMeAndKillMeLater -d jenkins
    sleep 1
    docker stop exitMeAndKillMeLater
    sleep 1
}
setup2

echo Slet containere der er termineret
docker rm $(docker ps --filter status=exited -q)
