#!/usr/bin/env bash
source ~/.profile
source ././../../common.sh

function waitUntilCreatedSoICanStopAndRemoveThem
{
    echo "Waiting a little before we stop and kill"
    sleep 2
}

docker events &
EVENTS_PID=$!

docker run -d --name ubuntu ubuntu
UBUNTU_PID=$!

docker run -d --name jenkins jenkins:$JENKINS_TAG
JENKINS_PID=$!

waitUntilCreatedSoICanStopAndRemoveThem
docker stop jenkins ubuntu && docker rm jenkins ubuntu

kill $UBUNTU_PID
kill $EVENTS_PID
kill $JENKINS_PID

