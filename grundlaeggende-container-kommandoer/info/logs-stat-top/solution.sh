#!/usr/bin/env bash

source ././../../../common.sh

function cleanup
{
    docker rm -f min-jenkins
}

trap cleanup EXIT
cleanup


echo Start en navngiven jenkins i baggrunden, og eksponer containerens port 8080 på hostens port 9999
docker run --name min-jenkins -d -p 9999:8080 jenkins:$JENKINS_TAG

echo Monitorer logs så du hele tiden ser hvad der sker i jenkins
xterm -hold -e "docker logs -f min-jenkins ; read" &
KILL_1=$!

echo Undersøg hvor meget load er der på containeren
xterm -hold -e "docker stats min-jenkins ; read" &
KILL_2=$!

echo Hvilke processer kører i containeren ?
xterm -hold -e "docker top min-jenkins ; read" &
KILL_3=$!

echo Vent lidt og ryd op
sleep 5

kill $KILL_1
kill $KILL_2
kill $KILL_3
