#!/usr/bin/env bash

source ././../../common.sh

function cleanup
{
    echo Removing previous containers, if any exists
    docker rm -f en-jenkins-container 2> /dev/null
}

cleanup

echo "Start en navngiven jenkins i baggrunden med porte eksponeret i den dynamiske port range"
docker run -d -P --name en-jenkins-container jenkins:$JENKINS_TAG

echo "Load jenkins ind i firefox ved at inspicere docker efter den dynamiske port"
echo The port is: $(docker inspect --format='{{(index (index .NetworkSettings.Ports "8080/tcp") 0).HostPort}}' en-jenkins-container)

cleanup
