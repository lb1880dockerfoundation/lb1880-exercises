#!/usr/bin/env bash

source ../../common.sh

function cleanup
{
    echo "Cleaning up"
    docker rm -f micro-ci base-jenkins 2> /dev/null
    rm -rf jobs 2> /dev/null
}

trap cleanup EXIT
cleanup

# Solution starter her:

echo Creating jobs folder, which has the uid of the current user. On single user systems \
its 1000, which is also the id of the jenkins user inside the container
mkdir jobs
echo start jenkins
docker run -d -P --name base-jenkins --mount source=$PWD/jobs,target=/var/jenkins_home/jobs,type=bind jenkins:$JENKINS_TAG

commonWaitUntillJenkinsStarts

echo "se i logs for at sikre jer at jenkins er startet op:"
docker logs --tail 5 base-jenkins

echo "Fake the creation of a job inside jenkins"
mkdir jobs/first_job
cp config.xml jobs/first_job/config.xml

echo "stop jenkins"
docker stop base-jenkins

# her kunne man gemme opsætning i filsystem / repositorie

echo "start ny jenkins med jobs fra den gamle jenkins"

docker run -d --name micro-ci --mount source=$PWD/jobs,target=/var/jenkins_home/jobs,type=bind -P jenkins:$JENKINS_TAG
echo "There is no automatic test that this actually works. Create one if you want 8)"

cleanup
