#!/usr/bin/env bash

function cleanup
{
    docker stop jenkins-1 jenkins-2
    docker rm jenkins-1 jenkins-2
    docker volume rm source
}
trap cleanup EXIT
cleanup

echo Start jenkins med delt filsystem
docker run -P -d --name jenkins-1 --mount source=source,target=/banana jenkins
docker exec --user root jenkins-1 bash -c "echo hello > /banana/hello.txt"
docker run -P -d --name jenkins-2  --volumes-from jenkins-1 jenkins

docker stop jenkins-1
docker rm -v jenkins-1
# See filesystem
docker volume inspect source

echo Hvad er der paa alpine-1
docker exec jenkins-2 cat /banana/hello.txt

cleanup
