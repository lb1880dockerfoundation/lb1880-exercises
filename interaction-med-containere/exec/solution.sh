#!/usr/bin/env bash

source ././../../common.sh

function cleanup
{
    docker rm -f min-jenkins
}

trap cleanup EXIT
cleanup


docker stop min-jenkins
docker rm min-jenkins

# When using -P to set port, you can see which port was assigned with
# docker ps
docker run -d --name min-jenkins -P jenkins:$JENKINS_TAG



# I denne løsning afvikles kommandoerne direkte, istedet for at gøre det mens man er logget ind
# Hvis man er logget ind, skal man køre kommandoerne i kommandoprompten i jenkins,
# dvs først gøre:
#docker exec -ti min-jenkins bash

commonWaitUntillJenkinsStarts

# hvilken bruger er jeg
echo ""
echo "On jenkins I am user"
docker exec -ti min-jenkins  id


# inspicer jobs katalog
echo ""
echo "On jenkins we these jobs:"
docker exec -ti min-jenkins ls /var/jenkins_home/jobs


# Lav et par jobs via UI

# Se de jobs du har lavet i filsystemet
echo ""
echo "On jenkins we now have these jobs:"
docker exec -ti min-jenkins ls /var/jenkins_home/jobs

# Slet jobs folderen
echo ""
echo "Deleting jobs:"
docker exec min-jenkins rm -rf /var/jenkins_home/jobs

# Genstart Jenkins
echo ""
echo "Restaring jenkins so we can see that the deleted jobs are gone:"
docker restart min-jenkins

# Se i UI at du har lavet nogle jobs
