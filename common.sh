#!/usr/bin/env bash

# Excersises inherit from this script

# See also
# http://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if [ -z ${JENKINS_TAG+x} ]; then
    echo "JENKINS TAG is unset, setting it to 1.651.2-alpine";
    export JENKINS_TAG=1.651.2-alpine
else
    echo "var is set to '$var'";
fi

if [ -z ${DOCKER_HOSTNAME+x} ]; then
    echo "DOCKERHOST is unset, setting it to localhost";
    export DOCKER_HOSTNAME=localhost
else
    echo "var is set to '$var'";
fi


function commonWaitUntillJenkinsStarts
{
    # Not used
    CONTAINER_ID=$1
    echo "Waiting 15 seconds for jenkins to start"
    sleep 15
}
export -f commonWaitUntillJenkinsStarts
