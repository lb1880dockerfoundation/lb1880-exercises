#!/usr/bin/env bash

docker-compose -f docker-compose.yml -f docker-compose-v1.yml -f docker-compose-v2.yml -f docker-compose-v3.yml up -d
docker-compose -f docker-compose.yml -f docker-compose-v1.yml -f docker-compose-v2.yml -f docker-compose-v3.yml ps
docker-compose -f docker-compose.yml -f docker-compose-v2.yml -f docker-compose-v3.yml up --remove-orphans -d
docker-compose -f docker-compose.yml -f docker-compose-v2.yml -f docker-compose-v3.yml down


