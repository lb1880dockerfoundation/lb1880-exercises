#!/bin/bash

docker-compose -f docker-compose.yml -f prod-compose.yml up -d
OVERWRITTEN=$(docker-compose -f docker-compose.yml -f prod-compose.yml run db env | grep overwritten_password)
[[ -z "$OVERWRITTEN" ]] && { echo "Password not overwritten" ; exit 1; }
docker-compose -f docker-compose.yml -f prod-compose.yml down
