#!/bin/bash

function  cleanup
{
 docker stop jwproxy jenkins
}
trap cleanup EXIT
cleanup

# You need to do something like
# echo 127.0.0.1 jenkins.dockerhost.example.org >> /etc/hosts
# or the equivivalent on windows
# to get the proxy to accept the connection
docker run --rm --name jwproxy -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
docker run --rm --name jenkins -d -e VIRTUAL_HOST=jenkins.dockerhost.example.org -e VIRTUAL_PORT=8080 jenkins:1.651.2-alpine

