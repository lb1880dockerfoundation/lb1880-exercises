#!/usr/bin/env bash

function sleepUntilPostgresIsStarted {
    echo sleepUntilPostgresIsStarted
    sleep 15
}

docker build -t image-med-database .
CONTAINER_ID=$(docker run -d image-med-database)

sleepUntilPostgresIsStarted

docker exec  -u postgres $CONTAINER_ID psql postgres -l
docker stop $CONTAINER_ID
docker rm $CONTAINER_ID
docker rmi image-med-database
