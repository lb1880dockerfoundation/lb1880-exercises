#!/usr/bin/env bash

docker network create -d bridge my-network
docker run -itd --net=my-network --name=c1 busybox
docker run -itd --net=my-network --name=c2 busybox

docker exec c2 ping c1 &
EVENTS_PID=$!

sleep 3
kill $EVENTS_PID

docker stop c1 c2
docker rm c1 c2
docker network rm my-network
