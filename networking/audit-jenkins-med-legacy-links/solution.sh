#!/usr/bin/env bash

function cleanup
{
    docker rm -f db jenkins
}

trap cleanup EXIT
cleanup

docker run --name db -d -e  POSTGRES_USER=audit -e POSTGRES_PASSWORD=audit -e POSTGRES_DATABASE=audit postgres:9.4
docker run --name jenkins --link=db -d jenkins

docker exec jenkins cat /etc/hosts
docker exec jenkins env
