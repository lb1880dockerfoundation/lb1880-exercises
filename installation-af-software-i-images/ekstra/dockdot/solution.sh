#!/usr/bin/env bash

docker build -t dockdot-apt-get .

docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz images --dot | docker run --rm -i --name=dockdot -v "$PWD":/input dockdot-apt-get -Tpng -o output.png