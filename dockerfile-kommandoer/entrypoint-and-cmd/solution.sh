#!/usr/bin/env bash

docker build -t entrypoint-and-cmd .
echo show help
docker run --rm entrypoint-and-cmd
echo show etc
docker run --rm entrypoint-and-cmd /etc
docker rmi -f entrypoint-and-cmd
